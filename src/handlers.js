const cmp = require('semver-compare');
const debug = require('debug')('handlers');
const Boom = require('boom');
const latest = process.env.LATEST_VERSION || '0.0.0';

function versionHandler(version, next) {
    const res = cmp(version, latest);
    debug(`Latest version is ${latest}, comparison result ${res}`);
    if (res > 0) {
        debug(`Unknown version ${version}`);
        return next(Boom.badRequest('Unknown version'));
    }
    if (res === 0) {
        debug('Latest version');
        return next({
            status: 'OK',
            latestVersion: latest
        });
    }
    debug('Old version');
    return next({
        status: 'Update needed',
        latestVersion: latest
    });
}  

module.exports = {
    versionHandler
};