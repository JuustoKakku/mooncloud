require('dotenv').config();
const debug = require('debug')('index');

const Hapi = require('hapi');
const server = new Hapi.Server();
const routes = require('./routes');

server.connection({
    host: '0.0.0.0',
    port: process.env.PORT || '3000',
    routes: {
        cors: {
            headers: ['Accept', 'Authorization', 'Content-Type', 'If-None-Match', 'Accept-language', 'Moonlight-Client']
        }
    }
});

routes.forEach(route => server.route(route));

server.start((err) => {
    if (err) {
        throw err;
    }
    debug(`Mooncloud running at ${server.info.url}`);
});