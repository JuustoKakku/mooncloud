const Joi = require('joi');
const {
    versionHandler
} = require('./handlers');
const debug = require('debug')('routes');

const routes = [
    {
        method: 'GET',
        path: '/api/v1/update',
        handler: (request, reply) => {
            debug(`Version query for ${request.query.version}`);
            versionHandler(request.query.version, reply);
        },
        config: {
            validate: {
                query: {
                    version: Joi.string().regex(/^\d+.\d+.\d+$/).required()
                }
            }
        }
    }
];

module.exports = routes;